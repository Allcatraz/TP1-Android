package ca.csf.mobile1.tp1.activity;

import android.os.Bundle;
import android.support.annotation.RequiresPermission;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import ca.csf.mobile1.tp1.R;
import ca.csf.mobile1.tp1.chemical.compound.*;
import ca.csf.mobile1.tp1.chemical.element.ChemicalElement;
import ca.csf.mobile1.tp1.chemical.element.ChemicalElementRepository;

import java.io.*;

public class MainActivity extends AppCompatActivity
{

    private View rootView;
    private EditText inputEditText;
    private TextView outputTextView;
    private ChemicalCompoundFactory chemicalCompoundFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rootView = findViewById(R.id.rootView);
        inputEditText = (EditText) findViewById(R.id.inputEditText);
        outputTextView = (TextView) findViewById(R.id.outputTextView);


        ChemicalElementRepository repository = new ChemicalElementRepository();

        try
        {
            InputStream inputStream = getResources().openRawResource(R.raw.chemical_elements);
            Reader reader = new InputStreamReader(inputStream);
            BufferedReader buffer = new BufferedReader(reader);
            String line = buffer.readLine();
            while (line != null)
            {
                String[] params = new String[4];
                String temp = "";
                int index = 0;
                for (int i = 0; i < line.length(); i++)
                {
                    if (line.charAt(i) != ',')
                    {
                        temp += line.charAt(i);
                    }
                    else
                    {
                        params[index] = temp;
                        temp = "";
                        index++;
                    }
                }
                line = buffer.readLine();
                params[index] = temp;
                repository.add(new ChemicalElement(params[0], params[1], Integer.parseInt(params[2]), Double.valueOf(params[3])));
            }
        }
        catch (Exception e)
        {
            Snackbar.make(rootView, R.string.readingError, Snackbar.LENGTH_LONG).show();
        }
        chemicalCompoundFactory = new ChemicalCompoundFactory(repository);
    }




    public void onSaveInstanceState(Bundle savedInstanceState)
    {

        savedInstanceState.putString(getString(R.string.outputTextViewSaver), inputEditText.getText().toString());
        savedInstanceState.putString(getString(R.string.inputEditTextSaver), outputTextView.getText().toString());
        super.onSaveInstanceState(savedInstanceState);
    }
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        inputEditText.setText(savedInstanceState.getString(getString(R.string.outputTextViewSaver)));
        outputTextView.setText(savedInstanceState.getString(getString(R.string.inputEditTextSaver)));
    }


    public void onComputeButtonClicked(View view)
    {
        try
        {
            outputTextView.setText( getString(R.string.text_output, chemicalCompoundFactory.createFromString( inputEditText.getText().toString() ).getWeight() ) );
        }
        catch (EmptyFormulaException e)
        {
            Snackbar.make(rootView, R.string.text_empty_formula, Snackbar.LENGTH_LONG).show();
        }
        catch (IllegalClosingParenthesisException e)
        {
            Snackbar.make(rootView, R.string.text_illegal_closing_parenthesis, Snackbar.LENGTH_LONG).show();
        }
        catch (MissingClosingParenthesisException e)
        {
            Snackbar.make(rootView, R.string.text_missing_closing_parenthesis, Snackbar.LENGTH_LONG).show();
        }
        catch (MisplacedExponentException e)
        {
            Snackbar.make(rootView, R.string.text_misplaced_exponent, Snackbar.LENGTH_LONG).show();
        }
        catch (EmptyParenthesisException e)
        {
            Snackbar.make(rootView, R.string.text_empty_parenthesis, Snackbar.LENGTH_LONG).show();
        }
        catch (UnknownChemicalElementException e)
        {
            Snackbar.make(rootView, getString(R.string.text_unknown_chemical_element, e.getElement()), Snackbar.LENGTH_LONG).show();
        }
        catch (IllegalCharacterException e)
        {
            Snackbar.make(rootView, getString(R.string.text_illegal_character, e.getCharacter()), Snackbar.LENGTH_LONG).show();
        }





    }

}
